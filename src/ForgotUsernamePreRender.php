<?php

namespace Drupal\forgot_username;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

/**
 * The PreRender class ForgotUsernamePreRender.
 */
class ForgotUsernamePreRender implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['forgotUsernameUserLoginBlockPreRender'];
  }

  /**
   * Determines the forgot username link.
   *
   * @return array
   *   The forgot username link.
   */
  public static function forgotUsernameUserLoginBlockPreRender(array $build): array {
    $build['content']['forgot_username'] = [
      '#type' => 'link',
      '#title' => t('Forgot username'),
      '#url' => Url::fromRoute('forgot_username.username', [], [
        'attributes' => [
          'title' => t('Forgot username'),
          'class' => ['forgot-username-link'],
        ],
      ]),
    ];
    return $build;
  }

}
