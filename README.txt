CONTENTS OF THIS FILE
---------------------

* INTRODUCTION
* REQUIREMENTS
* RECOMMENDED MODULES
* INSTALLATION
* CONFIGURATION
* TROUBLESHOOTING
* MAINTAINERS

Introduction
------------
Let's face it, users forget passwords. Also, most users also forget their
usernames. This module was designed to email them their username.

Requirements
------------
None

Installation
------------
Install and enable this module just like you would any Drupal module.

Configuration
-------------
No configuration is needed for this module to work.

Troubleshooting
---------------
Your email will most likely land in spam folder. This is not the module's 
fault. Take necessary precautions.

Maintainers
-----------
Tolga Ozses (Kartagis) - https://www.drupal.org/u/kartagis

This project is partly sponsored by binbiriz.

Specialized in consulting and planning of Drupal powered sites, binbiriz 
offers installation, development, theming, customization, and hosting
to get you started.
